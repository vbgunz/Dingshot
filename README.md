# Dingshot SourceMod L4D2 Plugin

If you enjoy SourceMod and its community or if you rely on it - please don't just be a vampire's anal cavity and help them out. Help them meet their monthly goal [here](http://sourcemod.net/donate.php) and don't be too proud to throw them some pocket change if that's all you got.

Thanks :)

## License

Dingshot a SourceMod L4D2 Plugin
Copyright (C) 2016  Victor B. Gonzalez

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About

Dingshot for L4D2 is oddly satisfying. It basically plays a "ding" on a headshot and a "DING" on a kill shot to the head. It's very simple and only watches for headshots. If any headshots are made, a sound is played. The sound that plays is customizable with the **ds_headshot** and **ds_killshot** cvars.

## Install

- Drop dingshot.smx into **.../addons/sourcemod/plugins**
- sm plugins load dingshot (or restart the server)
- Done

## Usage

While Dingshot is enabled, there's really nothing else to do but to enjoy those confirmed kills with a little ding and hopefully you'll get a few big dings :)

### Cvars and dingshot.cfg

The cfg is located at **.../cfg/sourcemod/dingshot.cfg** and contains 2 cvars for customizing sounds. You can find more sounds at a location similiar to **../left4dead2/sound/** but be advised **NOT ALL SOUNDS WILL WORK FOR SOME REASON**.

- ds_headshot  // sound bite to play on headshot
- ds_killshot  // sound bite to play on a killshot to the head

## Thanks

A good friend **Coleo** originally came up with the concept and wrote the plugin but never released it. I got tired of bothering the man for a copy and figured why wait, I'll try my hand at it (I really liked it). This is basically what he put me onto and I believe in sharing good things :)

Thanks to **dYZER** and their plugin [\[L4D2\] Headshots in a row](https://forums.alliedmods.net/showthread.php?p=1239957). From the sourcecode I found some great examples. Thanks mate!

## Reaching out to me

I love L4D2, developing, testing and running servers more than I like playing the game. Although I do enjoy the game and it is undoubtedly my favorite game, it is the community I think I love the most. It's always good to meet new people with the same interest :)

- [My Steam Profile](http://steamcommunity.com/id/buckwangs/)
- [My Dingshot GitLab Page](https://gitlab.com/vbgunz/Dingshot)
- [My SourceMod Thread](https://forums.alliedmods.net/showthread.php?t=293719)
